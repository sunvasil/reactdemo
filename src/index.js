import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


class FormItem extends React.Component {
    
    render() {
        return (
            <div>
                <input type="text" name={this.props.inputFldName} />
                <div className="addAnotherRow" onClick={() => this.props.addAnotherRow()}>+</div>
                <div className="clearBoth"></div>
            </div>
        )
    }
}

class ResponseText extends React.Component{
    render() {
        return (
            <div>{this.props.msg}</div>
        );
    }
}

class FormCollector extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items : [
                {fldName: "fldName_" + Math.random().toString().substring(2,10)}
            ], 
            msg:null
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    addAnotherRow () {
        const items = this.state.items;
        this.setState({items: items.concat(
            [{fldName: "fldName_" + Math.random().toString().substring(2,10)}]
        )});
    }
    handleSubmit(e) {
        e.preventDefault();
        const data = new FormData(e.target);
        fetch('http://test.loc/', {
            crossDomain:true,
            method: 'POST',
            body: data,
        }).then(res => res.json()).then((result) => {
            let msg = 'Had been sent: ';
            Object.keys(result).map((objectKey, index) => {
                var value = result[objectKey];
                msg += objectKey + ': ' + value + '; '
            });
            this.setState({msg: msg});
        });
    }
    render() {
        return (
            <div className="formCollector">
                <h1>Demo</h1>
                <form onSubmit={this.handleSubmit}>
                    {
                        this.state.items.map((item) => (
                            <FormItem key={item.fldName} 
                                inputFldName={item.fldName} addAnotherRow={() => this.addAnotherRow()} />
                        ))
                    }
                <input type="submit" name="submitBtn" />
                </form>
                <div>
                <ResponseText msg={this.state.msg} />
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <FormCollector />,
    document.getElementById('root')
);